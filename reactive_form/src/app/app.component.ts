import { Component } from '@angular/core';
import {FormBuilder, Validator, Validators } from '@angular/forms';
import { passwordValidator } from './shared/password.validator';
import { forbiddenNameValidator } from './shared/user-name.validator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
constructor (private fb : FormBuilder){}

registrationForm = this.fb.group({
  userName : ['', [Validators.required, Validators.minLength(5),forbiddenNameValidator(/admin/)]],
  password : [''],
  confirmPassword : [''],
  address : this.fb.group ({
    city : [''],
    state : [''],
    postalCode : ['']
  })    
}, {Validators : passwordValidator});

  // registrationForm = new FormGroup({
  //   userName : new FormControl('Rohan'),
  //   password : new FormControl(''),
  //   confirmPassword : new FormControl(''),
  //   address : new FormGroup({
  //     city : new FormControl(''),
  //     state : new FormControl(''),
  //     postalCode : new FormControl('')
  //   })
  // });

  loadApiData(){
    this.registrationForm.setValue({
      userName : 'Rahul',
      password : 'abcd',
      confirmPassword : 'abcd',
      address : {
        city : "Mumbai",
        state : "Maharashtra",
        postalCode : "400080"
      }
    });
  }
}
