import { Component } from '@angular/core';
import { User } from './user';
import { EnrollmentService } from './enrollment.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  topics = ['Angular','React','View']
  topicHasError = true
  isSubmitted = false
  errorMessage : any;
  userModel = new User('Rohan','rohan@gmail.com',8082796629,'default','',true)

  constructor(private _enrollmentService : EnrollmentService){

  }
  validateTopic(value : any){
    if(value === 'default'){
      this.topicHasError = true
    }else{
      this.topicHasError = false
    }

  }
  onSubmit(){
    this.isSubmitted = true
    console.log(this.userModel)
    this._enrollmentService.enroll(this.userModel).subscribe(
      data => this.errorMessage = data.statusText,
      error => this.errorMessage = error.statusText
    )
    alert('Form has been submitted successfully')
  }
}
